FROM node:13.12.0-alpine AS build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY . .
RUN rm -rfv ./node_modules
RUN npm i --quiet
RUN yarn build

FROM nginx:1.17.8-alpine

COPY --from=build /app/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]