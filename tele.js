function TeleLoop() {
    setTimeout(async function () {

        var unreadSelector = $("li.im_dialog_wrap > a.im_dialog > div.im_dialog_meta span.im_dialog_badge:not(:contains('0'))");

        var unreadChanels = $.map(
            unreadSelector.parent().parent().children('div.im_dialog_message_wrap').children('div.im_dialog_peer').children('span[my-peer-link="dialogMessage.peerID"]'),
            function (element) {
                return $(element).text()
            })
            .join("#%$")
            .split('#%$');

        console.log('Ping Loop');


        const channels = {
            "Резидент": "rezident_ua",
            "Легитимный": "legitimniy",
            "Анатолий Шарий": "ASupersharij",
            "ЗеРада": "ZeRada1",
            "Женщина с косой": "skosoi",
            "Тёмный Рыцарь": "dark_k",
            "\"СдалиСняли\" Харьков Аренда": "SdaliSnyali",
            "Хуевый": "hueviykharkov",
            "TUT.BY новости": "tutby_official"
        }

        function sendMessage(messages) {
            if (messages.length) {
                console.log('Message was sent');
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8089/message",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        "accept": "application/json",
                        "Access-Control-Allow-Origin": "*"
                    },
                    cache: false,
                    data: JSON.stringify(messages),
                    done: function (resp) {
                        console.log('qeqweqweqwe');
                    },
                    success: function (resp) {
                        console.log('zczxczxcxzc');
                    }
                });

            }
        }

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        if (unreadSelector.length > 0) {
            var photos = [];

            for (var i = 0; i < unreadChanels.length; i++) {
                let channel = channels[unreadChanels[i]];
                if (channel !== null && channel !== undefined) {

                    location.href = location.href.replace(new RegExp('im\?.*'), 'im?p=@' + channel);
                    await sleep(5000);

                    function getBlobFromUrl(bUrl) {
                        return new Promise((resolve, reject) => {
                            let xhr = new XMLHttpRequest()
                            xhr.responseType = 'blob'
                            xhr.addEventListener('load', event => {
                                if (xhr.status === 200) {
                                    resolve(xhr.response)
                                } else {
                                    reject(new Error('Cannot retrieve blob'))
                                }
                            })

                            xhr.open('GET', bUrl, true)
                            xhr.send()
                        })
                    }

                    function fromBlobToBase64(blob) {
                        return new Promise((resolve, reject) => {
                            let reader = new FileReader()
                            reader.addEventListener('loadend', event => {
                                resolve(reader.result)
                            })
                            reader.readAsDataURL(blob)
                        })
                    }

                    let blobUrl = $("li.im_dialog_wrap.active > a.im_dialog > .im_dialog_photo > .im_dialog_photo").attr("src");

                    getBlobFromUrl(blobUrl).then(fromBlobToBase64).then(result => {
                        var messageWrap = $('.im_history_messages_peer:not(.ng-hide)').children('.im_history_message_wrap').last().children('.im_message_outer_wrap').children('.im_message_wrap').children('.im_content_message_wrap');
                        var messageStructure = messageWrap.children('.im_message_body').children('div[my-message-body=historyMessage]');

                        var photoSrc = messageStructure.children('.im_message_media').children()
                            .children('div[message-id=messageId]').children('.im_message_photo_thumb')
                            .children('.im_message_photo_thumb').attr("src");

                        if (photoSrc !== null && photoSrc !== undefined) {
                            getBlobFromUrl(photoSrc).then(fromBlobToBase64).then(photo => {
                                photos.push(photo);
                            })
                                .then(() => {
                                    var unreadBreakpoint = $('.im_history_message_wrap:has(.im_message_unread_split)');

                                    if (unreadBreakpoint !== null && unreadBreakpoint !== undefined) {
                                        const messagesWraps = $('.im_history_message_wrap');
                                        for (let i = 0; i < messagesWraps.length; i++) {
                                            if (messagesWraps[i] == unreadBreakpoint[0]) {
                                                console.log(i);
                                            }
                                        }
                                    }

                                    var messages = [];
                                    var textMessage = messageStructure.children('.im_message_text').text();
                                    if (!textMessage.length) {
                                        textMessage = '';
                                    } else {
                                        console.log(textMessage);
                                    }

                                    var photoMessage = messageStructure.children('.im_message_media').children('div[ng-switch="::media._"]').children('div[message-id="messageId"]').children('.im_message_photo_caption').text();
                                    if (!photoMessage.length) {
                                        photoMessage = '';
                                    } else {
                                        console.log(photoMessage);
                                    }

                                    var messageSplitSeparator = messageWrap.children('.im_message_from_photo')

                                    messages.push({
                                        "textMessage": textMessage,
                                        "photoMessage": photoMessage,
                                        "channel": channel,
                                        "messageSplitSeparator": messageSplitSeparator.length > 0,
                                        "avatar": result
                                    });
                                    console.log('Messages and photos:');
                                    console.log(messages);
                                    console.log(photos);
                                    console.log('-------------:');
                                    sendMessage(messages);
                                    location.href = location.href.replace(new RegExp('im\?.*'), 'im?p=@herzegovina_flor');
                                });
                        } else {
                            var messages = [];
                            var textMessage = messageStructure.children('.im_message_text').text();
                            if (!textMessage.length) {
                                textMessage = '';
                            } else {
                                console.log(textMessage);
                            }

                            var photoMessage = messageStructure.children('.im_message_media').children('div[ng-switch="::media._"]').children('div[message-id="messageId"]').children('.im_message_photo_caption');
                            if (!photoMessage.length) {
                                photoMessage = '';
                            } else {
                                console.log(photoMessage);
                            }

                            var messageSplitSeparator = messageWrap.children('.im_message_from_photo')

                            messages.push({
                                "textMessage": textMessage,
                                "photoMessage": photoMessage,
                                "channel": channel,
                                "messageSplitSeparator": messageSplitSeparator.length > 0,
                                "avatar": result
                            });
                            console.log('Only messages:');
                            console.log(messages);
                            console.log('-------------:');
                            sendMessage(messages);
                            location.href = location.href.replace(new RegExp('im\?.*'), 'im?p=@herzegovina_flor');
                        }
                    })
                    await sleep(5000);
                }
            }

            $("li.im_dialog_wrap > a.im_dialog > div.im_dialog_meta span.im_dialog_badge:not(:contains('0'))").attr('class', 'span im_dialog_badge ng-hide')

            $.map($("li.im_dialog_wrap > a.im_dialog > div.im_dialog_meta span.im_dialog_badge:not(:contains('0'))"),
                function (element) {
                    var el = $(element);
                    el.attr('class', 'span im_dialog_badge ng-hide');
                    el.text('0');
                    return element;
                }
            )
        }

        TeleLoop();
    }, 10000)
}

TeleLoop();
