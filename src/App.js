import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import styles from './App.module.css';
import FreshPage from './pages/FreshPage/FreshPage';
import MessageInfoPage from './pages/MessageInfoPage/MessageInfoPage';
import PopularPage from './pages/PopularPage/PopularPage';
import AboutPage from './pages/AboutPage/AboutPage';
import SearchPage from './pages/SearchPage/SearchPage';
import TopicMessagesPage from './pages/TopicMessages/TopicMessagesPage';

const App = () => {

  const script = document.createElement("script");

  script.src = "https://www.google.com/recaptcha/api.js";
  script.async = true;

  document.body.appendChild(script);

  return (
    <div className={styles.main}>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={FreshPage} exact />
          <Route path="/message/:id" component={MessageInfoPage} exact />
          <Route path="/search" component={SearchPage} exact />
          <Route path="/popular" component={PopularPage} exact />
          <Route path="/about" component={AboutPage} exact />
          <Route path="/topic" component={TopicMessagesPage} exact />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
