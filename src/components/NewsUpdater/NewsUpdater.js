import React, { useEffect } from 'react';
import styles from './NewsUpdater.module.css';

const NewsUpdater = ({ messages }) => {

    const [upToDateMessages, setUpToDateMessages] = React.useState(true);

    useEffect(() => {
        setInterval(function () {
            const latestDate= messages[0].timeCreation;
            if (upToDateMessages) {
                fetch(`http://localhost:8089/isUpToDate?date=${latestDate !== null ? latestDate : ''}`, {
                    method: 'GET',
                    crossDomain: true,
                    headers: {
                        'Access-Control-Allow-Origin': '*'
                    }
                })
                    .then(res => res.json())
                    .then(res => {
                        setUpToDateMessages(res === true);
                    })
            }
        }, 3000);
    }, [messages, upToDateMessages, setUpToDateMessages]);


    return !upToDateMessages ? (
        <div className={styles.wrapper}>
            <button className={styles.updateButton} onClick={() => window.location.reload()}>Update Feed</button>
        </div>
    ) : (<></>)

}

export default NewsUpdater;
