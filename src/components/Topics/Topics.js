import React, { useEffect } from 'react';
import styles from './Topics.module.css';
import getColorForTopic from '../../util/TopicColorPalete'
import getLocalizedValue from '../../util/Localization'

const Topics = ({ showTopics }) => {

    const [topics, setTopics] = React.useState([]);

    useEffect(() => {
        fetch(`http://localhost:8089/messages/topics`, {
            method: 'GET',
            crossDomain: true,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(res => res.json())
            .then(res => {
                setTopics(res);
            })
    }, []);

    const showTopicsElements = () => {
        const elements = [];

        if (topics.length) {
            topics.forEach(topic => elements.push((
                <div className={styles.topic}>
                    <span className={styles.circle} style={{ color: getColorForTopic(topic) }}></span>
                    <a href={`/topic?topic=${topic}`} className={styles.topicLink}>{getLocalizedValue(topic)}</a>
                </div>
            )));
        }

        return elements;
    }

    return showTopics && topics.length ? (
        <div className={styles.topics}>
            {showTopicsElements()}
        </div>
    ) : (<></>);
}

export default Topics;