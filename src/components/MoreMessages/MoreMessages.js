import React from 'react';
import styles from './MoreMessages.module.css';

const MoreMessages = ({ messages, setMessages, url }) => {

    const [loading, setLoading] = React.useState(false);

    const loadMoreMessages = () => {
        setLoading(true);
        fetch(`${url}?skip=${messages.length}`, {
            method: 'GET',
            crossDomain: true,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(res => res.json())
            .then(res => {
                const newFeedMessages = messages.concat(res);
                setMessages(newFeedMessages);
                setLoading(false);
            })
    }

    return !loading ? (<div className={styles.wrapper}>
        <button className={styles.updateButton} onClick={loadMoreMessages}>More messages</button>
    </div>) : (<></>);

}

export default MoreMessages;
