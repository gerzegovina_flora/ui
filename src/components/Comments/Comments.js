import React, { useEffect } from 'react';
import $ from 'jquery';
import Identicon from 'react-identicons';
import { FiArrowUp } from 'react-icons/fi';
import { FiArrowDown } from 'react-icons/fi';
import Skeleton from 'react-loading-skeleton';

import styles from './Comments.module.css';

const Comment = ({ comment }) => {

    let voteFunction;
    let commentId;
    let widget;

    const widgets = {};

    const voteUp = () => {
        const selector = $(`#comment${commentId}`)
        const votes = Number.parseInt(selector.text(), 10) + 1;
        selector.text(votes);
        fetch(`http://localhost:8089/comment/like`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: commentId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
    }

    const voteDown = () => {
        const selector = $(`#comment${commentId}`)
        const votes = Number.parseInt(selector.text(), 10) - 1;
        selector.text(votes);
        fetch(`http://localhost:8089/comment/dislike`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: commentId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
    }

    function onChange(value) {
        if (value) {
            voteFunction();

            $(`#captchaComment${commentId}`).hide();
            voteFunction = null;
            commentId = null;
        }
    }

    const renderCaptha = (commentId) => {
        try {
            console.log('asdasd');
            widget = window.grecaptcha.render(`captchaComment${commentId}`, {
                'sitekey': '6Le6fLwZAAAAAIIzZUpXRPlyu1EP9XaH2ChS9uZ3',
                'callback': onChange,
            });
            widgets[commentId] = widget
        } catch (e) {
            window.grecaptcha.reset(widgets[commentId], {
                'sitekey': '6Le6fLwZAAAAAIIzZUpXRPlyu1EP9XaH2ChS9uZ3',
                'callback': onChange,
            });
        }
    }

    return (
        <div className={styles.commentWrapper}>
            <div className={styles.comment}>
                <div className={styles.commentHeader}>
                    <Identicon string={comment.author} size={50} />
                    <div className={styles.commentMetaInfo}>
                        <div className={styles.commentName}>{comment.author}</div>
                        <div className={styles.commentDate}>{comment.formattedTimeCreation}</div>
                    </div>
                </div>
                <div className={styles.commentContent}>
                    {comment.text}
                </div>
            </div>
            <div className={styles.commentVote}>
                <a href="#" className={styles.voteUp}
                    rel="noopener noreferrer"
                    onClick={(e) => {
                        e.preventDefault();
                        voteFunction = voteUp;
                        commentId = comment.id
                        $(`#captchaComment${comment.id}`).show();
                        renderCaptha(comment.id);
                    }}>
                    <FiArrowUp />
                </a>
                <span className={styles.voteNumber} id={`comment${comment.id}`}>{comment.votes}</span>
                <a href="#" className={styles.voteDown}
                    rel="noopener noreferrer"
                    onClick={(e) => {
                        e.preventDefault();
                        voteFunction = voteDown;
                        commentId = comment.id
                        $(`#captchaComment${comment.id}`).show();
                        renderCaptha(comment.id);
                    }}><FiArrowDown /></a>
            </div>
            <div id={`captchaComment${comment.id}`} className={styles.messageCaptcha}>
            </div>
        </div>
    )
}

const Comments = ({ messageId }) => {

    const [comments, setComments] = React.useState([]);
    const [commentText, setCommentText] = React.useState('');
    const [commnetSort, setCommentSort] = React.useState('popular');
    const [commentLoading, setCommentLoading] = React.useState(true);

    const loadComments = (sortType) => {
        setCommentLoading(true);
        fetch(`http://localhost:8089/comments/${sortType}?skip=0&messageId=${messageId}`, {
            method: 'GET',
            crossDomain: true,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(res => res.json())
            .then(res => {
                setComments(res);
                setCommentLoading(false);
            })
    }

    useEffect(() => {
        loadComments('popular');
    }, []);

    const sendComment = () => {
        fetch(`http://localhost:8089/comment`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                text: commentText,
                messageId: messageId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => res.json())
            .then(res => {
                setComments([res].concat(comments));
                setCommentText('');
            })
    }

    const showComments = () => {
        const elements = [];

        if (comments.length) {
            comments.forEach(comment => elements.push((<Comment comment={comment} />)));
        }

        return elements;
    }

    return (
        <div className={styles.wrap}>
            <div className={styles.sortOptions}>
                <div className={styles.sortItem}>
                    <a href="#"
                        onClick={() => {
                            setCommentSort('popular');
                            loadComments('popular');
                        }}
                        className={commnetSort === 'popular' ? styles.sortItemLinkSelected : styles.sortItemLink}
                    >Popular</a>
                </div>
                <div className={styles.sortItem}>
                    <a href="#"
                        onClick={() => {
                            setCommentSort('date')
                            loadComments('date');
                        }}
                        className={commnetSort === 'date' ? styles.sortItemLinkSelected : styles.sortItemLink}
                    >By date</a>
                </div>
            </div>
            {commentLoading ? (<Skeleton height={100} />) :
                (<div className={styles.comments}>
                    <div className={styles.commentInputWrapper}>
                        <div className={styles.commentBlock}>
                            <textarea className={styles.commentInput}
                                value={commentText}
                                onChange={(e) => setCommentText(e.target.value)}
                            />
                            <div className={styles.commentSubmit}>
                                <button className={styles.commentSendButton} onClick={sendComment}>Send comment</button>
                            </div>
                        </div>
                    </div>
                    {showComments()}
                </div>)}
        </div>
    );
}

export default Comments;
