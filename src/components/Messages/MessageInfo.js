import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom'
import { FiArrowUp, FiArrowDown } from 'react-icons/fi';
import { ReactTinyLink } from 'react-tiny-link'

import styles from './Messages.module.css';

const MessageInfo = ({ message }) => {

    let voteFunction;
    let messageId;

    const voteUp = (messageId) => {
        const selector = $(`#votes${messageId}`)
        const votes = Number.parseInt(selector.text(), 10) + 1;
        selector.text(votes);
        fetch(`http://localhost:8089/like`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: messageId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                console.log(res);
            })
    }

    const voteDown = (messageId) => {
        const selector = $(`#votes${messageId}`)
        const votes = Number.parseInt(selector.text(), 10) - 1;
        selector.text(votes);
        fetch(`http://localhost:8089/dislike`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: messageId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                console.log(res);
            })
    }

    const showLinks = (message) => {
        const links = [];
        for (let i = 0; i < message.links.length; i++) {
            const link = message.links[i];
            links.push(
                (<ReactTinyLink
                    cardSize="small"
                    maxLine={1}
                    minLine={1}
                    url={link}
                />)
            );
        }
        return links;
    }

    const showHashTags = (message) => {
        const links = [];
        for (let i = 0; i < message.hashTags.length; i++) {
            const hash = message.hashTags[i];
            links.push(<Link to={`/search?q=${hash}`} className={styles.hashLink}>{hash}</Link>);
        }
        return links;
    }

    const showMediaFiles = (message) => {
        const mediaFiles = [];
        for (let i = 0; i < message.mediaFiles.length; i++) {
            mediaFiles.push(<img src={`${message.mediaFiles[i]}`} />);
        }
        return mediaFiles;
    }

    function onChange(value) {
        if (value) {
            voteFunction(messageId);
            window.grecaptcha.reset();
            $(`#captcha${messageId}`).hide();
            voteFunction = null;
            messageId = null;
        }
    }

    return message !== null ? (
        <div className={styles.messageInfo}>
            <div className={styles.contentWrapper} >
                <div className={styles.messageHeader}>
                    <img src={message.avatar} className={styles.messageAvatar} />
                    <div className={styles.commentDate}>{message.formattedTimeCreation}</div>
                </div>
                <div className={styles.messageContent}>
                    {message.textMessage}
                    <div className={styles.links}>
                        {showLinks(message)}
                    </div>
                    <div className={styles.hashTags}>
                        {showHashTags(message)}
                    </div>
                    {showMediaFiles(message)}
                </div>
                <div className={styles.metaInfo}>
                    <div>
                        Источник: <a className={styles.channelName} href={`https://t.me/${message.channel}`} target="_blank" rel="noopener noreferrer">{message.channel}</a>
                    </div>
                    <div className={styles.vote}>
                        <a href="#" className={styles.voteUp} onClick={(e) => {
                            e.preventDefault();
                            voteFunction = voteUp;
                            messageId = message.id
                            $(`#captcha${message.id}`).show();
                        }}>
                            <FiArrowUp />
                        </a>
                        <span className={styles.voteNumber, message.votes > 0 ? styles.voteNumberHight : message.votes === 0 ? styles.voteNumber : styles.voteNumberLow} id={`votes${message.id}`}>{message.votes}</span>
                        <a href="#" className={styles.voteDown} onClick={(e) => {
                            e.preventDefault();
                            voteDown(message.id);
                        }}><FiArrowDown /></a>
                        <div id={`captcha${message.id}`} className={styles.messageCaptcha}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ) : (<></>);
}

export default MessageInfo;
