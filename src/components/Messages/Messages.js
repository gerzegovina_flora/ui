import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom'
import { AiOutlineComment } from 'react-icons/ai';
import { FiArrowUp, FiArrowDown } from 'react-icons/fi';
import getColorForTopic from '../../util/TopicColorPalete'
import getLocalizedValue from '../../util/Localization'

import NewsUpdater from '../NewsUpdater/NewsUpdater';
import styles from './Messages.module.css';

const Message = ({ message }) => {

    let voteFunction;
    let messageId;
    let widget;

    const widgets = {};

    const voteUp = (messageId) => {
        const selector = $(`#votes${messageId}`)
        const votes = Number.parseInt(selector.text(), 10) + 1;
        selector.text(votes);
        fetch(`http://localhost:8089/like`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: messageId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                console.log(res);
            })
    }

    const voteDown = (messageId) => {
        const selector = $(`#votes${messageId}`)
        const votes = Number.parseInt(selector.text(), 10) - 1;
        selector.text(votes);
        fetch(`http://localhost:8089/dislike`, {
            method: 'POST',
            crossDomain: true,
            dataType: "json",
            body: JSON.stringify({
                id: messageId
            }),
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                console.log(res);
            })
    }

    function onChange(value) {
        if (value) {
            voteFunction(messageId);
            window.grecaptcha.reset();
            $(`#messageCaptcha${messageId}`).hide();
            voteFunction = null;
            messageId = null;
        }
    }

    const renderCaptha = (messageId) => {
        try {
            widget = window.grecaptcha.render(`messageCaptcha${messageId}`, {
                'sitekey': '6Le6fLwZAAAAAIIzZUpXRPlyu1EP9XaH2ChS9uZ3',
                'callback': onChange,
            });
            widgets[messageId] = widget
        } catch (e) {
            window.grecaptcha.reset(widgets[messageId], {
                'sitekey': '6Le6fLwZAAAAAIIzZUpXRPlyu1EP9XaH2ChS9uZ3',
                'callback': onChange,
            });
        }
    }

    const showHashTags = (message) => {
        const links = [];
        for (let i = 0; i < message.hashTags.length; i++) {
            const hash = message.hashTags[i];
            links.push(<Link ey={`hashTag${i}`} to={`/search?q=${hash}`} className={styles.hashLink}>{hash}</Link>);
        }
        return links;
    }

    const showMediaFiles = (message) => {
        const mediaFiles = [];
        for (let i = 0; i < message.mediaFiles.length; i++) {
            mediaFiles.push(<img key={`mediaImg${i}`} src={`${message.mediaFiles[i]}`} width="440" height="300" />);
        }
        return mediaFiles;
    }

    return message !== null ? (
        <div className={styles.message}>
            <img src={message.avatar} className={styles.messageAvatar} />
            <div className={styles.contentWrapper} >
                <div className={styles.messageHeader}>
                    <a className={styles.channelName} href={`https://t.me/${message.channel}`} rel="noopener noreferrer" target="_blank">{message.channel}</a>
                    <div className={styles.commentDate}>{message.formattedTimeCreation}</div>
                </div>
                <div className={styles.messageContent}>
                    {message.textMessage}
                </div>
                {showMediaFiles(message)}
                <div className={styles.hashTags}>
                    {showHashTags(message)}
                </div>
                <div className={styles.metaInfo}>
                    <div className={styles.commentAndTopic}>
                        <a className={styles.commentLink} href={`/message/${message.id}`}>
                            <AiOutlineComment />({message.numberOfComments})
                        </a>
                        <div className={styles.topic} style={{ background: getColorForTopic(message.topicClass) }}>
                            {getLocalizedValue(message.topicClass)}
                        </div>
                    </div>
                    <div className={styles.vote}>
                        <a href="#" className={styles.voteUp}
                            rel="noopener noreferrer"
                            onClick={(e) => {
                                e.preventDefault();
                                voteFunction = voteUp;
                                messageId = message.id
                                $(`#messageCaptcha${message.id}`).show();
                                renderCaptha(message.id);
                            }}>
                            <FiArrowUp />
                        </a>
                        <span id={`vote${message.id}`} className={styles.voteNumber, message.votes > 0 ? styles.voteNumberHight : message.votes === 0 ? styles.voteNumber : styles.voteNumberLow}>{message.votes}</span>
                        <a href="#" className={styles.voteDown}
                            rel="noopener noreferrer"
                            onClick={(e) => {
                                e.preventDefault();
                                voteFunction = voteDown;
                                messageId = message.id
                                $(`#messageCaptcha${message.id}`).show();
                                renderCaptha(message.id);
                            }}><FiArrowDown /></a>
                        <div id={`messageCaptcha${message.id}`} className={styles.messageCaptcha}></div>
                    </div>
                </div>
            </div>
        </div>
    ) : (<></>);
}


const Messages = ({ messages, setMessages, enableUpdater = false }) => {


    const showMessages = () => {
        const elements = [];

        if (messages.length) {
            messages.forEach(message => elements.push((<Message message={message} />)));
        }

        return elements;
    }

    return (
        <div>
            {enableUpdater && messages !== undefined && messages.length > 0 ? (<NewsUpdater messages={messages} />) : (<></>)}
            <div className={styles.messageWrapper}>
                {showMessages()}
            </div>
        </div>
    );
}

export default Messages;
