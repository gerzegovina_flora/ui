import React from 'react';
import { useHistory } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import { FcSearch } from 'react-icons/fc';
import { GiHamburgerMenu } from 'react-icons/gi';

import styles from './Header.module.css';

const Header = ({ showTopics, setShowTopics }) => {

    const history = useHistory();
    const [searchText, setSearchText] = React.useState('');
    const [showSearch, setShowSearch] = React.useState(false);

    return (
        <div className={styles.headerWrap}>
            {!isMobile && (<div className={styles.headerSearch}>
                <GiHamburgerMenu size={20} onClick={() => setShowTopics(!showTopics)} />
                <input type="text" className={styles.search} placeholder="Поиск" onKeyDown={
                    (e) => {
                        if (e.keyCode === 13) {
                            history.push("/search?q=:text");
                        }
                    }
                }
                    onChange={e => setSearchText(e.target.value)}
                    value={searchText}
                ></input>
            </div>)}


            {isMobile ? (
                <div className={styles.headerOptionsBlock}>
                    <div className={styles.headerOptions}>
                        <div className={styles.item}><a className={styles.headerLink} href={"/"}>Свежее</a></div>
                        <div className={styles.item}><a className={styles.headerLink} href={"/popular"}>Популярное</a></div>
                        <div className={styles.item}><a className={styles.headerLink} href={"/about"}>Про TelNet</a></div>
                    </div>
                    <div className={styles.searchMobile} onClick={() => setShowSearch(!showSearch)}>
                        <FcSearch size={40} />
                    </div>
                </div>
            ) : (
                    <div className={styles.headerOptions}>
                        <div className={styles.item}><a className={styles.headerLink} href={"/"}>Свежее</a></div>
                        <div className={styles.item}><a className={styles.headerLink} href={"/popular"}>Популярное</a></div>
                        <div className={styles.item}><a className={styles.headerLink} href={"/about"}>Про TelNet</a></div>
                    </div>
                )}

            {isMobile && showSearch && (<div className={styles.headerSearch}>
                <input type="text" className={styles.search} placeholder="Поиск" onKeyDown={
                    (e) => {
                        if (e.keyCode === 13) {
                            history.push("/search?q=:text");
                        }
                    }
                }
                    onChange={e => setSearchText(e.target.value)}
                    value={searchText}
                ></input>
            </div>)}

        </div>
    );
}

export default Header;
