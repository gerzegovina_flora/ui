import React from 'react';
import styles from './MessageInfoPage.module.css';

import Header from '../../components/Header/Header';
import MessageInfo from '../../components/Messages/MessageInfo';
import Comments from '../../components/Comments/Comments';

class MessageInfoPage extends React.Component {

    getMessageInfo = () => {
        fetch(`http://localhost:8089/message/${this.props.match.params.id}`, {
            method: 'GET',
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    message: res,
                });
            });
    }

    constructor(props) {
        super(props);

        this.state = {
            message: null,
        };

        this.getMessageInfo = this.getMessageInfo.bind(this);
        this.getMessageInfo();
    }

    render() {
        return (
            <div className={styles.main}>
                <Header />
                <MessageInfo message={this.state.message} />
                <Comments messageId={this.props.match.params.id}/>
            </div>
        )
    }
}

export default MessageInfoPage;