import React, { useEffect } from 'react';
import styles from './FreshPage.module.css';

import Header from '../../components/Header/Header';
import Messages from '../../components/Messages/Messages';
import Topics from '../../components/Topics/Topics';
import MoreMessages from '../../components/MoreMessages/MoreMessages';

const FreshPage = () => {

    const [messages, setMessages] = React.useState([]);
    const [showTopics, setShowTopics] = React.useState(false);

    const MESSAGE_UPDATE_URL = `http://localhost:8089/messages`;

    useEffect(() => {
        fetch(MESSAGE_UPDATE_URL, {
            method: 'GET',
            crossDomain: true,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(res => res.json())
            .then(res => {
                setMessages(res);
            })
    }, [MESSAGE_UPDATE_URL, setMessages]);

    return (
        <div className={styles.main}>
            <Header showTopics={showTopics} setShowTopics={setShowTopics}/>
            <div className={styles.content}>
                <div>
                    <Topics showTopics={showTopics} />
                </div>
                <div>
                    <Messages messages={messages} setMessages={setMessages} enableUpdater />
                    <MoreMessages messages={messages} setMessages={setMessages} url={MESSAGE_UPDATE_URL} />
                </div>
            </div>
        </div>
    );
}

export default FreshPage;