import React from 'react';
import styles from './SearchPage.module.css';

import Header from '../../components/Header/Header';

const SearchPage = () => {

    // const textWord = new URLSearchParams(document.location.search.substring(1)).get("q");

    return (
        <div className={styles.main}>
            <Header />
            Search
        </div>
    );
}

export default SearchPage;