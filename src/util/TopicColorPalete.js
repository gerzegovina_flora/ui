
const colors = {
    "TELEGRAM": "#70d6ff",
    "COVID": "#ff70a6",
    "POLITICS": "#ff9770",
    "OTHER": "#ffd670",
    "SOROS": "#f20089",
    "BELARUS": "#ee4266",
    "CORRUPTION": "#3bceac",
    "WAR": "#8ac926",
    "ELECTIONS": "#41ead4"
}

const getColorForTopic = (topic) => {
    return colors[topic]
}

export default getColorForTopic;