
const localizations = {
    "TELEGRAM": "Телеграм",
    "COVID": "COVID",
    "POLITICS": "Политика",
    "OTHER": "Другое",
    "SOROS": "Соросята",
    "BELARUS": "Беларусь",
    "CORRUPTION": "Коррупция",
    "WAR": "Война",
    "ELECTIONS": "Выборы"
}

const getLocalizedValue = (value) => localizations[value];

export default getLocalizedValue;